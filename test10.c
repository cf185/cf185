#include<stdio.h>

void input(int *a,int *b)
{
	printf("Enter two numbers\n");
	scanf("%d%d",a,b);
}

void arithmetic_operations(int *x,int *y,int *add,int *sub,int *mul,float *div,int *rem)
{
	*add=*x+*y;
	*sub=*x-*y;
	*mul=*x * *y;
	*div=(float)*x/(*y);
	*rem=*x%*y;
}

void output(int x,int y,int add,int sub,int mul,float div,int rem)
{
	printf("Arithmetic operations on two number %d and %d are\n",x,y);
	printf("%d+%d=%d\n",x,y,add);
	printf("%d-%d=%d\n",x,y,sub);
	printf("%d*%d=%d\n",x,y,mul);
	printf("%d/%d=%.2f\n",x,y,div);
	printf("Remainder of %d/%d=%d\n",x,y,rem);
}

int main()
{
	int a,b,add,sub,mul,rem;
	float div;
	input(&a,&b);
	arithmetic_operations(&a,&b,&add,&sub,&mul,&div,&rem);
	output(a,b,add,sub,mul,div,rem);
	return 0;
}
