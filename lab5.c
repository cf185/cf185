#include<stdio.h>

struct swap
{
	int value,position;
};

typedef struct swap Swap;

void input(int n,int arr[n])
{
	int i;
	printf("Enter the array elements\n");
	for(i=0;i<n;i++)
		{
			scanf("%d",&arr[i]);
		}
}

void dupilcate(int n,int arr[n],int arr_new[n])
{
    int i;
    for(i=0;i<n;i++)
        arr_new[i]=arr[i];   
}

void check(int n,int arr[n],Swap *max,Swap *min,int arr_new[n])
{
	int i;
	for(i=1;i<n;i++)
		{
			if(arr[i]>max->value)
			{
				max->value=arr[i];
				max->position=i;
			}
			if(arr[i]<min->value)
			{
				min->value=arr[i];
				min->position=i;
			}
		}
	arr_new[min->position]=max->value;
	arr_new[max->position]=min->value;
}

void output(int n,int arr[n],int arr_new[n])
{
	int i;
	printf("Array elements before interchanging:\n");
	for(i=0;i<n;i++)
		printf("%d  ",arr[i]);
	printf("\n");
	printf("Array elements after interchanging:\n");
	for(i=0;i<n;i++)
		printf("%d  ",arr_new[i]);
}

int main()
{
	int n;
	printf("Enter the number of inputs:");
	scanf("%d",&n);
	int arr[n];
	input(n,arr);
	Swap max={arr[0],0};Swap min={arr[0],0};
	int arr_new[n];
	dupilcate(n,arr,arr_new);
	check(n,arr,&max,&min,arr_new);
	output(n,arr,arr_new);
	return 0;
}