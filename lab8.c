#include<stdio.h>

struct Emp
{
	int Eid;
	char Ename[25];
	char DOJ[10];
	float salary;
};

struct Emp input()
{
	struct Emp e1;
	printf("Enter the employee ID:");
	scanf("%d",&e1.Eid);
	printf("Enter the employee name:");
	scanf("%s",e1.Ename);
	printf("Enter the date of join(DD/MM/YYYY):");
	scanf("%s",e1.DOJ);
	printf("Enter the employee salary:");
	scanf("%f",&e1.salary);
	return e1;
}

void output(struct Emp e)
{
	printf("\nEmployee id:%d\n",e.Eid);
	printf("Employee name:%s\n",e.Ename);
	printf("Employee date of join:%s\n",e.DOJ);
	printf("Employee id:%.2f\n",e.salary);
}

int main()
{
	struct Emp e;
	e=input();
	output(e);
	return 0;
}
