#include<stdio.h>

void input(char filename[])
{
	FILE *fp=fopen(filename,"w");
	char ch;
	printf("Enter the data to stored \n");
	while((ch=getchar())!=EOF)
	{
		putc(ch,fp);
	}
	fclose(fp);
}

void output(char filename[])
{
	FILE *fp=fopen(filename,"r");
	char ch;
	printf("The data in file\n");
	while((ch=getc(fp))!=EOF)
	{
		printf("%c",ch);
	}printf("\n");
}

int main()
{
	char filename[]="Input.txt";
	input(filename);
	output(filename);
	return 0;
}