#include<stdio.h>

int input()
{
    int n;
    printf("Enter the number of lines:");
    scanf("%d",&n);
    return n;
}

void output(int n)
{
    int i,j;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=i;j++)
            printf("%d",j);
        printf("\n");
    }
}

int main()
{
    int n;
    n=input();
    output(n);
    return 0;
}
