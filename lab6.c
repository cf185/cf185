#include<stdio.h>

void input(int m,int n,int matrix[m][n])
{
	int i,j;
	for(i=0;i<m;i++)
	{
		printf("Enter the marks scored by student %d\n",(i+1));
		for(j=0;j<n;j++)
			scanf("%d",&matrix[i][j]);
	}
}

void output(int n,int great[n])
{
	int i;
	for(i=0;i<n;i++)
	printf("Highest marks in course %d is %d\n",(i+1),great[i]);
}

void compute(int m,int n,int matrix[m][n],int great[n])
{
	int i,j,grt;
	for(j=0;j<n;j++)
	{
		grt=matrix[0][j];
		for(i=0;i<m;i++)
		{
			if(matrix[i][j]>grt)
				grt=matrix[i][j];
		}
		great[j]=grt;
	}
}

int main()
{
	int m,n;
	printf("Enter the number of rows:");
	scanf("%d",&m);
	printf("Enter the number of columns:");
	scanf("%d",&n);
	int matrix[m][n];
	int great[n];
	input(m,n,matrix);
	compute(m,n,matrix,great);
	output(n,great);
	return 0;
}
