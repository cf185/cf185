#include<stdio.h>

void input(int sales[4][3])
{
	int i,j;
	for(i=0;i<4;i++)
	{
		printf("Enter the sales of salesman %d\n",(i+1));
		for(j=0;j<3;j++)
			scanf("%d",&sales[i][j]);
	}
}

void total_sales_of_salesman(int sales[4][3])
{
	int i,j,tot;
	for(i=0;i<4;i++)
	{
		tot=0;
		for(j=0;j<3;j++)
			tot+=sales[i][j];
		printf("Total sales of salesman %d is %d\n",(i+1),tot);
	}
}

void total_sales_of_product(int sales[4][3])
{
	int i,j,tot;
	for(j=0;j<3;j++)
	{
		tot=0;
		for(i=0;i<4;i++)
			tot+=sales[i][j];
		printf("Total sales of product %d is %d\n",(j+1),tot);
	}
}

int main()
{
	int sales[4][3];
	input(sales);
	total_sales_of_salesman(sales);
	total_sales_of_product(sales);
	return 0;
}
