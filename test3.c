#include<stdio.h>
#include<math.h>
#include<stdlib.h>

struct complex
{
	float real,img;
};

typedef struct complex Complex;

void input(float *a,float *b,float *c)
{
	printf("Enter the coefficent of x^2:");
	scanf("%f",a);
	printf("Enter the coefficent of x:");
	scanf("%f",b);
	printf("Enter the constant:");
	scanf("%f",c);
}

float  compute(float a,float b,float c,float *root1,float *root2,Complex *roots)
{
    float d;
	d=((b*b)-(4*a*c));
    if(a!=0)
    {
	if(d>0)
	{
		*root1=(-b+sqrt(d))/(2*a);
		*root2=(-b-sqrt(d))/(2*a);
	}
	else if(d==0)
	{
		*root1=(-b)/(2*a);
		*root2=(-b)/(2*a);
	}
	else
	{   
		roots->real=(-b)/(2*a);
		roots->img=((float)sqrt(-d))/(2*a);
	}
    
    }
	else
	{
	    printf("Not a quadratic equation\n");
	    exit(0);
	}
	return d;
}

void output(float a,float b,float c,float root1,float root2,Complex roots,float d)
{
	if(d>0)
	{
		printf("Real and unequal roots \n"); 
		printf("%0.2fx^2 %0.2fx %0.2f \n",a,b,c);
		printf("Root1=%0.2f\n",root1);
		printf("Root2=%0.2f\n",root2);
		}
	else if(d==0)
	{
		printf("Real and equal root \n");
		printf("%0.2fx^2 %0.2fx %0.2f \n",a,b,c);
		printf("Root1=%0.2f\n",root1);
		printf("Root2=%0.2f\n",root2);
	}
	else 
	{
		printf("Imaginary roots\n");
		printf("%0.2fx^2  0.2fx %0.2f \n",a,b,c);
		printf("Root1=%0.2f + %0.2f i\n",roots.real,roots.img);
		printf("Root2=%0.2f - %0.2f i\n",roots.real,roots.img);
		
	}
     

}

int main()
{
	float a,b,c,d,root1,root2;
	Complex roots;
	input(&a,&b,&c);
	d=compute(a,b,c,&root1,&root2,&roots);
	output(a,b,c,root1,root2,roots,d);
	return 0;
}