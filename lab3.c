#include<stdio.h>

void input(float *a,float *b,float *c)
{
    printf("Enter three number\n");
    scanf("%f%f%f",a,b,c);
}

float compute(float a,float b,float c)
{
    if(a>=b&&a>=c)
      return a;
    else if(b>=c)
      return b;
    else    
      return c;
}

void output(float a,float b,float c,float d)
{
    printf("Among %0.2f %0.2f %0.2f numbers:Largest Number is %0.2f",a,b,c,d);
}

int main()
{
    float a,b,c,d;
    input(&a,&b,&c);
    d=compute(a,b,c);
    output(a,b,c,d);
    return 0;
}