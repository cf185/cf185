#include<stdio.h>
#include<math.h>    
void input(float *x1,float *y1,float *x2,float *y2)
{
	printf("Enter the first point coordinates\n");
	scanf("%f%f",x1,y1);
	printf("Enter the second point coordinates\n");
	scanf("%f%f",x2,y2);
}

void compute(float x1,float y1,float x2,float y2,float *d)
{
	*d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
}

void output(float x1,float y1,float x2,float y2,float d)
{
	printf("The distace of points (%f,%f) and (%f,%f) is:%f\n",x1,y1,x2,y2,d);
}

int main()
{
	float x1,x2,y1,y2,d;
	input(&x1,&y1,&x2,&y2);
	compute(x1,y1,x2,y2,&d);
	output(x1,y1,x2,y2,d);
	return 0;
}
