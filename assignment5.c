#include<stdio.h>

int input()
{
	int n;
	printf("Enter the number:");
	scanf("%d",&n);
	return n;
}

int reverse_of_number(int n)
{
	int x,sum=0;
	x=n;
	while(x!=0)
	{
		sum=sum*10+x%10;
		x=x/10;
	}
	return sum;
}

void output(int n,int sum)
{
	printf("The reverse of a number=%d is %d",n,sum);
}

int main()
{
	int n,sum;
	n=input();
	sum=reverse_of_number(n);
	output(n,sum);
	return 0;
}
