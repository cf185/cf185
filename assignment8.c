#include<stdio.h>
#include<string.h>

void input(char str[25])
{
	printf("Enter the string :");
	scanf("%s",str);
}

int length(char str[])
{
	int i=0,len=0;
	while(str[i]!='\0')
	{
		len++;
		i++;
	}
	return len;
}

void toLowerCase(char str[25],char string[25])
{
	int i;char str2[25];
	for(i=0;i<length(str);i++)
		str2[i]=str[i]+32;
	strcpy(string,str2);
}

void output(char str[25],char string[25])
{
	printf("The the %s to lower case is %s \n",str,string);
}

int main()
{
	char str[25],string[25];
	input(str);
	toLowerCase(str,string);
	output(str,string);
	return 0;
}
