#include<stdio.h>


int searching(int n,int arr[n],int key)
{
    int flag=0,beg=0,end=n-1;
    int mid=(beg+end)/2;
    while(beg<=end)
    {
        if(arr[mid]==key)
        {
            flag=1;
            break;
        }
        if(arr[mid]<key)
            beg=mid+1;
        else
            end=mid-1;
        mid=(beg+end)/2;
    }
    return flag;
}

void output(int n,int arr[n],int flag,int key)
{
    int i;
    printf("Array elements \n");
    for(i=0;i<n;i++)
        printf("%d   ",arr[i]);
    printf("\n\n");
    if(flag==1)
        printf("%d found in the array\n",key);
    else
        printf("%d not found\n",key);
}

int main()
{
    int arr[6]={2,5,7,8,15,18};
    int n=6;
    int key=5;
    int flag=searching(n,arr,key);
    output(n,arr,flag,key);
    return 0;
}
