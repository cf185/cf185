#include<stdio.h>

float input()
{
	float r;
	printf("Enter the radius of a circle \n");
	scanf("%f",&r);
    return r;
}
   
float compute(float r)
{
	float area;
	area=3.14*r*r;
    return area;
}

void output(float r,float area)
{
	printf("The area of circle of radius=%f:%f\n",r,area);
}

int main()
{
	float r,area;
	r=input();
	area=compute(r);
	output(r,area);
    return 0;
}
