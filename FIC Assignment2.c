#include<stdio.h>

int input()
{
    int n;
    printf("Enter the number of lines:");
    scanf("%d",&n);
    return n;
}

void output(int n)
{
    int i,j;char ch;
    for(i=1;i<=n;i++)
    {
        for(j=1,ch='A';j<=i;j++,ch++)
            printf("%c",ch);
        printf("\n");
    }
}

int main()
{
    int n;
    n=input();
    output(n);
    return 0;
}
