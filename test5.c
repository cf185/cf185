#include<stdio.h>

int input()
{
	int num;
	printf("Enter the number:");
	scanf("%d",&num);
	return num;
}

void compute(int num,int a[100],int *number)
{
	int i;
	for(i=1;i<=100;i++)
	{
		if(i%num==0)
		{
			a[*number]=i;
			*number+=1;
		}
	}
}

void output(int num,int a[100],int number)
{
	printf("Multiples of number=%d\n",num);
	int i;
	for(i=0;i<number;i++)
		printf("%d\n",a[i]);
	
}

int main()
{
	int n,number=0;
	int arr[100];
	n=input();
	compute(n,arr,&number);
	output(n,arr,number);
	return 0;
}
