#include<stdio.h>

void input(char str[])
{
	printf("Enter  the string to check for palindrome:");
	scanf("%s",str);
}

int lengthOfString(char str[])
{
	int i=0,len=0;
	while(str[i]!='\0')
	{
		len++;
		i++;
	}
	return len;
}


int palindrome(char str[])
{
	int i,res=1,length;
	length=lengthOfString(str);
	for(i=0;i<length/2;i++)
	{
		if(str[i]!=str[length-1-i])
		{
			res=0;
			break;
		}
	}
	return res;
}

void output(char str[],int result)
{
	if(result==1)
		printf("%s is palindrome\n",str);
	else
		printf("%s is not palindrome\n",str);
}

int main()
{
	char str[25];
	int result;
	input(str);
	result=palindrome(str);
	output(str,result);
	return 0;
}
