#include<stdio.h>

void input(char str1[25],char str2[25])
{
	printf("Enter the string 1:");
	scanf("%s",str1);
	printf("Enter teh string 2:");
	scanf("%s",str2);
}

int length(char str[])
{
	int i=0,len=0;
	while(str[i]!='\0')
	{
		len++;
		i++;
	}
	return len;
}

void concatenate(char str1[],char str2[],char str3[])
{
	int i,j;
	for(i=0;i<length(str1);i++)
		str3[i]=str1[i];
	for(j=0;j<length(str2);j++,i++)
		str3[i]=str2[j];
	str3[i]='\0';
}

void output(char str1[25],char str2[25],char str3[100])
{
	printf("Concatenation of %s and %s is %s\n",str1,str2,str3);
}

int main()
{
	char str1[25],str2[25],constr[100];
	input(str1,str2);
	concatenate(str1,str2,constr);
	output(str1,str2,constr);
	return 0;
}
