#include<stdio.h>
#include<stdlib.h>
void input(int *m,int *n)
{
	printf("Enter the starting number:");
	scanf("%d",m);
	printf("Enter the last number:");
	scanf("%d",n);
}

void compute(int m,int n,int a[n-m+1])
{
	int i,x;
	for(i=0,x=m;i<=(n-m);i++,x++)
		a[i]=x*x;
}

void output(int m,int n,int a[n-m+1])
{
	int i,x=m;
	for(i=0;i<=n-m;i++,x++)
		printf("%d*%d=%d\n",x,x,a[i]);
}

int main()
{
	int m,n;
	input(&m,&n);
	if(n>m)
	{
		int a[n-m+1];
		compute(m,n,a);
		output(m,n,a);
	}
	else
	{
	    printf("Not possible for entered values\n");
		exit(0);
	}
	return 0;
	
}