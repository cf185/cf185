#include<stdio.h>
void input(float *p,float *r,float *t)
{
	printf("Enter Principal Amount,Rate, and Time \n");
	scanf("%f %f %f",p,r,t);
}

int output(float si,float p,float r,float t);
float compute(float p,float r,float t);

int main()
{
	float p,r,t,res;
	input(&p,&r,&t);
	res=compute(p,r,t);
	output(res,p,r,t);
}

float compute(float p,float r,float t)
{
  float si;
  si=(p*t*r)/100;
  return si;
}

int output(float si,float p,float r,float t)
{
	printf("The SImple interest of Principal=%f Rate=%f Time=%f:%0.2f \n",p,r,t,si);
	return 0;
}


