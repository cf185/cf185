#include<stdio.h>

void input(int m,int n,int matrix[m][n])
{
	int i,j;
	for(i=0;i<m;i++)
	{
	    printf("Enter the %d row elements\n",(i+1));
	    for(j=0;j<n;j++)
	        scanf("%d",&matrix[i][j]);
	}
    
}

void compute(int m,int n,int matrix[m][n],int tmatrix[n][m])
{
	int i,j,k,l;
	for(j=0;j<n;j++)   
	{
		for(i=0;i<m;i++)
			{
				tmatrix[j][i]=matrix[i][j];
			}
	}
}

void output(int n,int m,int tmatrix[n][m])
{
	int i,j;
	printf("Transpose of the matrix \n");
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%d  ",tmatrix[i][j]);
		printf("\n");
	}
}

int main()
{
	int m,n;
	printf("Enter the rows of matrix:");
	scanf("%d",&m);
	printf("Enter the columns of the matrix:");
	scanf("%d",&n);
	int matrix[m][n];
	int tmatrix[n][m];
	input(m,n,matrix);
	compute(m,n,matrix,tmatrix);
	output(n,m,tmatrix);
	return 0;
}
