#include<stdio.h>

void input(int *hours,int *min)
{
	printf("Enter hours and minutes\n");
	scanf("%d%d",hours,min);
}

int compute(int hours,int min)   
{
	int res=hours*60+min;
	return res;
}

void output(int hours,int min,int res)
{
	printf("Hours:Minutes=%d:%d in Minutes=%d\n",hours,min,res);
}

int main()
{
	int hours,min,res;
	input(&hours,&min);
	res=compute(hours,min);
	output(hours,min,res);
	return 0;
}