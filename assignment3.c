#include<stdio.h>

void input(unsigned int sub[],int n)
{
	for(int i=0;i<n;i++)
	{
	printf("Enter  subject  marks %d:",(i+1));
	scanf("%u",&sub[i]);
	}
}

void compute(unsigned int sub[],char g[],int n)
{
	for(int i=0;i<n;i++)
	{
	char grade;
    if(sub[i]<40)
    grade='F';
	else if(sub[i]<50)
	 grade='E';
	else if(sub[i]<60)
	 grade='D';
	 else if(sub[i]<70)
	 grade='C';
	else if(sub[i]<80)
	 grade='B';
	else if(sub[i]<90)
	 grade='A';
	else if(sub[i]>=90&&sub[i]<=100)
		grade='S';
    else
        grade='\0';   
    g[i]=grade;
	}
	
}

void output(unsigned int arr[],char g[],int n)
{
	for(int i=0;i<n;i++)
	printf("Subject of marks %d=%u Grade=%c\n",(i+1),arr[i],g[i]);
	
}

int main()
{
	int n;
    printf("Enter number of subject:");
    scanf("%d",&n);
    unsigned int sub[n];char g[n];
    input(sub,n);
    compute(sub,g,n);
	output(sub,g,n);
	return 0;
	
}
