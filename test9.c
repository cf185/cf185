#include<stdio.h>

struct student
{
	int roll;
	char name[25];
	char section[4];
	char dept[25];
	float fees;
	float marks;
};

void input(struct student Student[2])
{
	int i;
	for(i=0;i<2;i++)
	{
		printf("Enter the roll number of student %d:",(i+1));
		scanf("%d",&Student[i].roll);
		printf("Enter the name of student %d:",(i+1));
		scanf("%s",Student[i].name);
		printf("Enter the section of student %d:",(i+1));
		scanf("%s",Student[i].section);
		printf("Enter the department of student %d:",(i+1));
		scanf("%s",Student[i].dept);
		printf("Enter the fees of student %d:",(i+1));
		scanf("%f",&Student[i].fees);
		printf("Enter the marks of student %d:",(i+1));
		scanf("%f",&Student[i].marks);
		printf("\n");
	}
}

int compute(struct student Student[2])
{
	int res=-1,i;
	for(i=0;i<1;i++)
	{
		if(Student[i].marks>Student[i+1].marks)
			res=0;
		else
			res=1;
	}
	return res;
}

void output(struct student Student[2],int res)
{
	printf("\nStudent %d has highest score\n",(res+1));
	printf("Roll number:%d\n",Student[res].roll);
	printf("Name :%s\n",Student[res].name);
	printf("Section:%s\n",Student[res].section);
	printf("Department:%s\n",Student[res].dept);
	printf("Fees:%.2f\n",Student[res].fees);
	printf("Total marks:%.2f\n",Student[res].marks);
}

int main()
{
	struct student Student[2];
	int flag;
	input(Student);
	flag=compute(Student);
	output(Student,flag);
	return 0;
}
