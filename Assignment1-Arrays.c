#include<stdio.h>

int searching(int arr[6],int *position)
{
    int i,flag=0,key=25;
    for(i=0;i<6;i++)
    {
        if(arr[i]==key)
        {
            flag=1;
            *position=i;
            break;
        }
    }
    return flag;
}

void output(int flag,int arr[6],int position)
{
    int i;
    printf("Given array:\n");
    for(i=0;i<6;i++)
        printf("%d  ",arr[i]);
    printf("\n\n");
    if(flag==1)
    {
        printf("Search successful\n");
        printf("Found at position:%d\n",position);
    }
    else
        printf("Unsuccessful\n");
}

int main()
{
    int flag,position,arr[]={22,14,30,25,11,43};
    flag=searching(arr,&position);
    output(flag,arr,position);
    return 0;
}