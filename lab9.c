#include<stdio.h>

void input(int *x,int *y)
{
	printf("Enter two numbers\n");
	scanf("%d%d",x,y);
}

void swap(int *x,int *y)
{
	int *temp;
	temp=x;
	x=y;
	y=temp;
}

void output(int x,int  y)
{
	printf("Before swapping X=%d Y=%d\n",y,x);
	printf("After swapping X=%d Y=%d\n",x,y);
}

int main()
{
	int x,y;
	input(&x,&y);
	swap(&x,&y);
	output(x,y);
	return 0;
}
  