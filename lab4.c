#include<stdio.h>

int input()
{
	int n;
	printf("Enter a number:");
	scanf("%d",&n);
	return n;
}

void sum_of_digits(int n,int *sum)
{
	int x=n;
	while(x!=0)
	{      
		*sum+=x%10;
		x=x/10;
	}
}

void output(int n,int sum)
{
	printf("Sum of digits of number=%d is %d \n",n,sum);
}

int main()
{
	int number,sum=0;
	number=input();
	sum_of_digits(number,&sum);
	output(number,sum);
	return 0;
	
}