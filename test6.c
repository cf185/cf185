#include<stdio.h>

void input(int n,int arr[n])
{
	int i;
	for(i=0;i<n;i++)
	 {
	 	printf("Enter the number %d:",(i+1));
		scanf("%d",&arr[i]);
	 }
}

float average(int n,int arr[n])
{
	float sum=0;
	int i;
	for(i=0;i<n;i++)
	 {
	 	sum=sum+arr[i];
	 }
	return (sum/n);
}

void output(float avg,int n)
{
	printf("The average of %d numbers=%.2f",n,avg);
}

int main()
{
	int n;
	printf("Enter the number of inputs:");
	scanf("%d",&n);
	int arr[n];
	input(n,arr);
	float avg=average(n,arr);
	output(avg,n);
	return 0;
	
}